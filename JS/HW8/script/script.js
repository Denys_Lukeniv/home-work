const text = document.getElementById('container')
const textPrice = document.createElement('p')
textPrice.innerHTML = "Price";
text.prepend(textPrice)
const input = document.createElement('input')
input.classList.add('input')
const container = document.querySelector('.container')
container.append(input)

const price = document.getElementById('price')

input.addEventListener('blur', () => {
    input.classList.remove("input:focus")
    if (input.value <= 0 || isNaN(input.value)) {
        let errorMessage = document.createElement('span')
        errorMessage.classList.add('error')
        errorMessage.innerHTML = "Please enter correct price"
        input.after(errorMessage)
        input.style.backgroundColor = 'red'
        input.addEventListener('focus', function () {errorMessage.remove()})
    } else {

        const price = document.getElementById('price')
        const priceMessage = document.createElement('span')
        const priceContainer = document.createElement('div')
        
        priceContainer.classList.add('priceContainer')
        priceMessage.classList.add('priceMessage')
        priceMessage.innerText = `Текущая цена: ${input.value} `
        price.prepend(priceContainer)
        priceContainer.prepend(priceMessage)

        const button = document.createElement('span')
        button.classList.add('button')
        button.innerHTML = "X"
        priceMessage.after(button)
        input.style.backgroundColor = 'white'
    }
})


document.addEventListener('click', (e) => {
    const button = document.querySelector('.button')
    const priceMessage = document.querySelector('.priceMessage')
    const priceContainer = document.querySelector('.priceContainer')

    if (e.target.classList.contains('button')){
    
    priceMessage.remove()
    button.remove()
    // priceContainer.remove()
    input.value = ''
    input.style.backgroundColor = 'white'}
})

