const slider = document.querySelector('.images-wrapper').children

let count = 0;

function showImages(){

    timer = setInterval(() => {
        if(count < slider.length){
            slider[count].classList.toggle('image-to-show')
            count++
        }
        if(count >= slider.length){
            count = slider.length % count 
        }
    }, 3000) 
    return timer
}
showImages()

const stopButton = document.querySelector('.stop')
const continueButton = document.querySelector('.continue')

continueButton.addEventListener('click', () => {
    showImages()
})

stopButton.addEventListener('click', () => {
    clearInterval(timer)
})

