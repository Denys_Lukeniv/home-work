document.body.addEventListener('keydown', (e) => {
    const buttonKey = document.querySelector('.btn-wrapper').children
    for (let i = 0; i < buttonKey.length; i++) {
        if(buttonKey[i].dataset.id === e.key.toLowerCase()){
            buttonKey[i].classList.add('btn-blue')
        } else{
            buttonKey[i].classList.remove('btn-blue')
        }
    }
})
