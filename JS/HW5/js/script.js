function createNewUser() {
    let name = prompt("Enter your first name")
    let surname = prompt("Enter your last name")
    let dateOfBirth = prompt("Enter your date of birth")
    dateOfBirth = dateOfBirth.split(".")
    let correctDateOfBirth = new Date(parseInt(dateOfBirth[2], 10), parseInt(dateOfBirth[1], 10) - 1 , parseInt(dateOfBirth[0]), 10);
    return {
        firstName: name,
        lastName: surname,
        birthday: correctDateOfBirth,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase()

        },
        getPassword: function () {
            return  (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear())

        },
        getAge: function (){
            let now = new Date()
            return Math.round((now - this.birthday)/(1000*60*60*24*365))
        }
    }
}

const user = createNewUser()
console.log(user.getPassword());
console.log(user.getAge());