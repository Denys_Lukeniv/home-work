function filterBy(array, dataType) {
    return array.filter(item => typeof item !== dataType);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));